import { Component } from '@angular/core';

@Component({
  selector: 'appphaser-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
