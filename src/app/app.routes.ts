import { Routes } from '@angular/router';
import { MainStartComponent } from '@src/app/pages/main-start/main-start.component';
import { MainLvl1Component } from '@src/app/pages/main-lvl1/main-lvl1.component'

export const routes: Routes = [

  {
      path: '',
      component: MainLvl1Component,
      pathMatch: 'full',
  },
  {
      path: 'main-lvl1',
      component: MainLvl1Component,
      pathMatch: 'full',
  },
];
