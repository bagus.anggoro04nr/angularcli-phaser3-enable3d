import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InlineSVGModule } from 'ng-inline-svg';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { MainStartComponent } from '@src/app/pages/main-start/main-start.component';
import { MainLvl1Component } from '@src/app/pages/main-lvl1/main-lvl1.component';


@NgModule({
  declarations: [
    AppComponent,
    MainStartComponent,
    MainLvl1Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InlineSVGModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
