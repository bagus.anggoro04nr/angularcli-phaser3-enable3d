import { Component, Injectable } from '@angular/core';
import 'Phaser';
import { Project, Scene3D  } from 'enable3d';
import { enable3d, Canvas, PhysicsLoader,
  ExtendedObject3D, THREE, ExtendedMesh } from '@enable3d/phaser-extension';

// declare var Phaser: any;
@Injectable({ providedIn: 'root' })
@Component({
  selector: 'appphaser-main-lvl1',
  templateUrl: './main-lvl1.component.html',
  styleUrls: ['./main-lvl1.component.css']
})
export class MainLvl1Component extends Scene3D {

  public background: any;
  public targetWidth: any;
  public targetHeight: any;
  public boxScale: any;
  public boxShape: any;
  public boxShapeAnother: any;
  public boxImg: any;
  public boxImgAnother: any;
  public boxExtr: any;
  public boxImgs: any[] = [];
  public boxPositions: any;
  public graphics: any;

  public posScreen: any[] = [];
  private posX: any;
  private posY: any;
  private rowCol: any;
  public location: Location;

  public colRow: any[] = [];

  public pictPosition: any;
  public pickedObject: any;

  constructor() {
    super({ key: 'MainLvl1Component'});

    this.targetWidth = window.outerWidth;
    this.targetHeight = window.outerHeight;


  }

   init() {
    this.renderer.setPixelRatio(1)
    this.renderer.setSize(window.innerWidth, window.innerHeight)

  }
   preload (){
        // console.log(this);
        // this.accessThirdDimension()
        this.load.preload('star', '../../assets/content/sprite/buttonstar.svg');
        this.load.preload('background', '../../assets/content/img/forest.png');
        this.load.preload('box', '../../assets/content/sprite/button_netral.svg');
  }

async create ()
  {

    this.warpSpeed('light', 'camera', '-sky', '-orbitControls', '-ground')
    this.camera.position.set(0, 0, 10)
    this.camera.lookAt(0, 0, 0)
    this.load.texture('background').then(background => (this.scene.background = background));
    // this.add.image(0, 0, 'background').setOrigin(0).setScale(8);
    // console.log(this.input);
    this.boxImg = location.origin + '/' + this.cache.get('box');
    this.boxImgAnother = location.origin + '/' + this.cache.get('star');
    // console.log(this.boxImg);

    this.boxShape = this.transform.fromSVGtoShape(this.boxImg);
    this.boxShapeAnother = this.transform.fromSVGtoShape(this.boxImgAnother);
    this.boxScale = 250;
    // console.log(this.boxShape[0]);
    // console.log(this.cache);

    // this.add.box({ y: 2 }, {  lambert: {color: 'deepskyblue' }} );
    // this.physics.add.existing('box');
// this.physics.add.box({ y: 10 }, { lambert: { color: 'hotpink' } })

    this.boxPositions = this.centerOfScreen(9);
      // console.log(this.boxPositions);
      // console.log(this.game.config.sceneConfig);
      // this.game.config.sceneConfig.pushdata();
    this.addObject();

    this.renderer.domElement.addEventListener("click", this.onClick.bind(this), true);


    this.renderer.render(this.scene, this.camera);
      // this.tweens.add({
      //     targets: this.logo,
      //     y: 350,
      //     duration: 1500,
      //     ease: 'Sine.inOut',
      //     yoyo: true,
      //     repeat: -1
      // })
  }

  update(time, delta) {


      this.boxImgs.forEach((boxExtr) => {
           boxExtr.rotation.y += 0.03;
           boxExtr.body.needUpdate = true;


        });

        if (this.pictPosition !== undefined && this.pictPosition !== '') {

          this.boxImgs.forEach((boxInPic) => {

               if (boxInPic.position.x == this.pictPosition.x && boxInPic.position.y == this.pictPosition.y ) {
                    var cloned = boxInPic.material.clone() // ini di gunakan buat ganti warna hanya satu object
                    cloned.color.setHex(0x7cfc00);
                    // boxInPic.material = cloned;
                    boxInPic.geometry.dispose();
                    boxInPic.material.dispose();
                    this.scene.remove( boxInPic );
                    this.addObjectAnother(this.pictPosition.x, this.pictPosition.y, cloned);
               }
          });

          this.pictPosition = '';
        }

   }

   addObjectAnother(posX, posY, materials) {

     let extrudeSettings = {
       steps: 2,
       depth: 16,
       bevelEnabled: true,
       bevelThickness: 1,
       bevelSize: 1,
       bevelOffset: 0,
       bevelSegments: 1
     };
     this.boxExtr = this.add.extrude({shape: this.boxShapeAnother}, extrudeSettings)
     this.boxExtr.name = `this.boxExtr`
     this.boxExtr.scale.set(10 / this.boxScale, 10 / -this.boxScale, 10 / this.boxScale)
     this.boxExtr.material = materials
     this.boxExtr.position.setX(posX)
     this.boxExtr.position.setY(posY)
     this.physics.add.existing(this.boxExtr, {shape: 'box', width: 2, height: 0.5, depth: 2})
     this.boxExtr.body.setCollisionFlags(6)
     this.boxImgs.push(this.boxExtr);


   }

   addObject() {
     const sensor = new ExtendedObject3D()
     sensor.position.setY(0)
     this.physics.add.existing(sensor, { mass: 1e-8, shape: 'box', width: 0.2, height: 0.2, depth: 0.2 })
     sensor.body.setCollisionFlags(4)

     this.boxPositions.forEach((pos, i) => {
       let posXs = JSON.parse(pos);
       // console.log(posXs);
       let extrudeSettings = {
         steps: 2,
         depth: 16,
         bevelEnabled: true,
         bevelThickness: 1,
         bevelSize: 1,
         bevelOffset: 0,
         bevelSegments: 1
       };
       this.boxExtr = this.add.extrude({shape: this.boxShape}, extrudeSettings)
       this.boxExtr.name = `this.boxExtr-${i}`
       this.boxExtr.scale.set(10 / this.boxScale, 10 / -this.boxScale, 10 / this.boxScale)
       this.boxExtr.material.color.setHex(0x00bfff)
       this.boxExtr.position.setX(posXs.x)
       this.boxExtr.position.setY(posXs.y)

       this.physics.add.existing(this.boxExtr, {shape: 'box', width: 2, height: 0.5, depth: 2})
       this.boxExtr.body.setCollisionFlags(6)
       this.boxImgs.push(this.boxExtr);
       this.physics.add.constraints.lock(this.boxExtr.body, sensor.body)

     });
   }

   onClick(e) {
     const pickPosition = {x: 0, y: 0};
     this.pictPosition = {x: 0, y: 0};
     var raycaster = new THREE.Raycaster();
     var pickedObjectSavedColor = 0x00bfff;

     const pos = this.getCanvasRelativePosition(event);
     pickPosition.x = (pos.x / this.renderer.domElement.width ) *  2 - 1;
     pickPosition.y = (pos.y / this.renderer.domElement.height) * -2 + 1;  // note we flip Y

     raycaster.setFromCamera(pickPosition, this.camera);
     const intersectedObjects = raycaster.intersectObjects(this.scene.children);
     if (intersectedObjects.length) {

        this.pickedObject = intersectedObjects[0].object;
        this.pictPosition.x = this.pickedObject.position.x
        this.pictPosition.y = this.pickedObject.position.y
    }

   };

  getCanvasRelativePosition(event) {
    const rect = this.renderer.domElement.getBoundingClientRect();
    return {
      x: (event.clientX - rect.left) * this.renderer.domElement.width  / rect.width,
      y: (event.clientY - rect.top ) * this.renderer.domElement.height / rect.height,
    };
  }


   centerOfScreen(data) {

     this.rowCol = this.numrow(data);
     // console.log(this.rowCol);
     this.posX = 0;
     this.posY = 2 * this.rowCol.row;

     for(let b = 0; b < data; b++) {

       for(let i=0; i < this.rowCol.row; i++) {

           if ( Number.isInteger(b/this.rowCol.col) === true) {

             this.posY -= 1;
             this.posX =  1 * this.rowCol.row;
           }

           for(let a = 0; a < this.rowCol.col; a++) {


             if ( Number.isInteger(a / this.rowCol.col) === true) this.posX -= 1;
             // console.log('x : ' + this.posX);
             this.posScreen[b] = '{"x":' + this.posX + ', "y":' + this.posY + '}';

           }

       }

     }
     // console.log(JSON.parse(JSON.stringify(this.posScreen)));
     return this.posScreen;

   }

   numrow(number) {
     let baris = 0;
     let a = 1;
     let column = 0;
     for(a = 1; a <= number; a++){

     if(Math.floor(number / a) >= a){

           baris = a;
           column = (number/a);

       }continue;

     }

     this.colRow['row'] = baris;
     this.colRow['col'] = column;

     return this.colRow;
   }

}

let config = {
    type: Phaser.AUTO,
    parent: 'lvl1',
    transparent: true,
    width: window.outerWidth,
    height: window.outerHeight,
    scenes: [
      MainLvl1Component
    ],
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'app-angular-phaser',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: window.outerWidth,
        height: window.outerHeight,
        pageAlignHorizontally: true,
        pageAlignVertically: true
    },
    physics: {
      default: 'arcade'
    },
    input: {
        keyboard: {
            target: window
        },
        mouse: {
            target: null,
            capture: true
        },
        activePointers: 1,
        touch: {
            target: null,
            capture: true
        },
        smoothFactor: 0,
        gamepad: false,
        windowEvents: true,
    },
    disableContextMenu: false,
    // Canvas: "lvl1"
};


// PhysicsLoader('../../assets/lib/', () => new Phaser.Game(config));
enable3d(() => new Project(config)).withPhysics('../../assets/lib');
