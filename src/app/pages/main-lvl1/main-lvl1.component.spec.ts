import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLvl1Component } from '@src/app/pages/main-lvl1/main-lvl1.component';

describe('MainLvl1Component', () => {
  let component: MainLvl1Component;
  let fixture: ComponentFixture<MainLvl1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainLvl1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLvl1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
