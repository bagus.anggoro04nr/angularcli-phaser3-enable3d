import { Component, OnInit, Injectable } from '@angular/core';
import { MainLvl1Component } from '@src/app/pages/main-lvl1/main-lvl1.component'
import { InlineSVGConfig } from 'ng-inline-svg';

@Injectable({ providedIn: 'root' })
@Component({
  selector: 'appphaser-main-start',
  templateUrl: './main-start.component.html',
  styleUrls: ['./main-start.component.css']
})
export class MainStartComponent extends InlineSVGConfig implements OnInit   {

  constructor(public mainLvl1: MainLvl1Component) {
    super();
    // If you don't want the directive to run on the server side.
    this.clientOnly = true;

    // If you want to bypass your HttpClient interceptor chain when fetching SVGs.
    this.bypassHttpClientInterceptorChain = true;
  }

  ngOnInit(): void {

  }

  toLvl1() {
    return MainLvl1Component;
  }



}
